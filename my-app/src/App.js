import React from 'react';
// import logo from './logo.svg';
import './Soal2/soal2.css';
import './App.css';
import { BrowserRouter as Router } from "react-router-dom";
import Routes from './Soal2/Route';

function App() {
  return (
    <>
     
      <Router>
        <Routes/>
      </Router>
    
    </>
  );
}

export default App;

