import React, {useState, useEffect} from "react"
import axios from "axios"


const MovieList = () => {  
  const [movieList, setMovieList] =  useState(null)
  const [input, setInput]  =  useState({Title: "", Description: "", Year: 0, Duration:"", Genre :"", Rating:"", id: null})

  useEffect( () => {
    if (movieList === null){
      axios.get(`http://backendexample.sanbercloud.com/api/movies`)
      .then(res => {
        setMovieList(res.data.map(el=>{ return {id: el.id, Title: el.Title, Description: el.Description, Year: el.Year, Duration : el.Duration, Genre : el.Genre, Rating : el.Rating }} ))
      })
    }
  }, [movieList])
  
  const handleDelete = (event) => {
    let idMovieList = parseInt(event.target.value)

    let newMovieList = movieList.filter(el => el.id !== idMovieList)

    axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idMovieList}`)
    .then(res => {
      console.log(res)
    })
          
    setMovieList([...newMovieList])
    
  }
  
  const handleEdit = (event) =>{
    let idMovieList = parseInt(event.target.value)
    let dataMovie = movieList.find(x=> x.id === idMovieList)
    setInput({title: dataMovie.Title, Description: dataMovie.Description, Year: dataMovie.Year, Duration : dataMovie.Duration, Genre : dataMovie.Genre, Rating : dataMovie.Rating,    id: idMovieList})
  }

  const handleChange = (event) =>{
    let typeOfInput = event.target.name
    switch (typeOfInput){
      case "Title":
      {
        setInput({...input, Title: event.target.value});
        break
      }
      case "Description":
      {
        setInput({...input, Description: event.target.value});
        break
      }
      case "Year":
      {
        setInput({...input, Year: event.target.value});
          break
      }
      case "Duration":
      {
        setInput({...input, Duration: event.target.value});
        break
      }  case "Genre":
      {
        setInput({...input, Genre: event.target.value});
        break
      }
      case "Rating":
        {
          setInput({...input, Rating: event.target.value});
          break
        }
    
      
    default:
      {break;}
    }
  }


  
  const handleSubmit = (event) =>{
    // menahan submit
    event.preventDefault()

    let Title = input.Title
    let Description = input.Description
    let Year = input.Year.toString()
    let Duration = input.Duration
    let Genre = input.Genre
    let Rating = input.Rating

    if (input.id === null){        
      axios.post(`http://backendexample.sanbercloud.com/api/movies`, {Title, Description,Year, Duration,Genre, Rating})
      .then(res => {
          setMovieList([
            ...movieList, 
            { id: res.data.id, 
              Title, 
              Description,
              Year,
              Duration,
              Genre,
              Rating
            }])
      })
    }else{
      axios.put(`http://backendexample.sanbercloud.com/api/movies/${input.id}`, {Title, Description,Year, Duration, Genre, Rating} )
      .then(() => {
          let movieList = movieList.find(el=> el.id === input.id)
          movieList.Title = Title
          movieList.Description = Description
          movieList.Year = Year
          movieList.Duration = Duration
          movieList.Genre = Genre
          movieList.Rating = Rating
          setMovieList([...movieList])
      })
    }

    // reset input form to default
    setInput({title: "", Description: "", Year: 0 , Duration: " ",Genre :"", Rating :"",id: null})

  }


  return(
    <>
    <section>
      <h1>Data Film</h1>
      <table style={{width: "100%", margin: "0 auto", display: "block"}}>
        <thead>
          <tr>
            <th>No</th>
            <th>Title</th>
            <th>Deskripsi</th>
            <th>Year</th>
            <th>Duration</th>
            <th>Genre</th>
            <th>Rating</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>

            {
              movieList !== null && movieList.map((item, index)=>{
                return(                    
                  <tr key={index}>
                    <td>{index+1}</td>
                    <td>{item.Title}</td>
                    <td>{item.Description}</td>
                    <td>{item.Year}</td>
                    <td>{item.Duration}</td>
                    <td>{item.Genre}</td>
                    <td>{item.Rating}</td>
                    <td>
                      <button onClick={handleEdit} value={item.id}>Edit</button>
                      &nbsp;
                      <button onClick={handleDelete} value={item.id}>Delete</button>
                    </td>
                  </tr>
                )
              })
            }
        </tbody>
      </table>
   {/* Form */}
      <h1>Form data Film</h1>

      <div style={{width: "100%", margin: "0 auto", display: "block"}}>
        <div style={{border: "1px solid #aaa", padding: "20px"}}>
          <form onSubmit={handleSubmit}>
            <label style={{float: "left"}}>
              Title:
            </label>
            <input style={{float: "right"}} type="text" required name="Title" value={input.title} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Deskripsi:
            </label>
            <textarea style={{float: "right"}} name="Description" value={input.Description} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Tahun :
            </label>
            <br/>
            <input style={{float: "right"}} type="number" name="Year" value={input.Year} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Durasi:
            </label>
            <input style={{float: "right"}} type="text" name="Duration" value={input.Duration} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Genre:
            </label>
            <input style={{float: "right"}} type="text" name="Genre" value={input.Genre} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Rating:
            </label>
            <input style={{float: "right"}} type="text" name="Rating" value={input.Rating} onChange={handleChange}/>
            <br/>
            <br/>
            <div style={{width: "100%", paddingBottom: "20px"}}>
              <button style={{ float: "right"}}>submit</button>
            </div>
          </form>
        </div>
      </div>
      </section>
    </>
  )
}

export default MovieList
