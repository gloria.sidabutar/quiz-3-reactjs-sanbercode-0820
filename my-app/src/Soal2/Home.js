import React, {Component} from 'react';
import './soal2.css';
import axios from "axios";


class Movie extends Component{
  render(){
    const img={
      width: "60%",
      "object-fit": "cover",
      height: "500px"
    }

    
    return (
      <div>
        <h4><b>{this.props.item.name}</b></h4> 
        <img src={this.props.item.photo} style={img} alt="Avatar"/>
          <p>Rating {this.props.item.Rating}</p> 
          <p>Durasi :{this.props.item.Durasi}</p> 
          <p>Genre :{this.props.item.Genre}</p> 
          <p>Deskripsi :{this.props.item.Deskripsi}</p> 
        </div>
    
    ) 
  }
}


function Home() {
  const data = [
    {name: "Avenger", photo: "https://upload.wikimedia.org/wikipedia/id/0/0d/Avengers_Endgame_poster.jpg",Rating : 9.5, Durasi: "4 jam", Genre: "Fiksi", Deskripsi : 
    ""},
    {name:"Dilan", photo: "https://akcdn.detik.net.id/visual/2018/02/09/7b2bde50-dda3-43ba-8e0c-f1656ba1a2f4_169.png?w=715&q=90",Rating : 9, Durasi: "2 jam", Genre: "Romantic", Deskripsi: ""}

  ]
  return (
    <>
      <section>
      <h1>DAFTAR FILM TERBAIK</h1>
      <div style={{width: "100%", margin: "0 auto"}}>
        {
          data.map((el, idx)=>{
            return(          
              <Movie key={idx} item={el}/>
            )
          })
        }
      </div>
      </section>
    </>
  );
}

export default Home;

