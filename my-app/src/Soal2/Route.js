import "./soal2.css"
import React from "react";
import { Switch, Route } from "react-router-dom";
import Nav from './Nav';
import Home from './Home';
import MovieList from './Movielist';
import About from  './About';

export default function App() {
  return (
      <>
           <Nav/>  
          <body>
            <Switch >
            <Route exact path="/Home" component={Home}>
              <Home/>
            </Route>
            <Route exact path="/About" >
              <About/>
            </Route>
            <Route exact path="/MovieList" component={MovieList}>
              <MovieList/>
            </Route>
          </Switch>
          <footer>
          <h5>copyright &copy; 2020 by Sanbercode</h5>
          </footer>

          </body>
          

      </>
  );
}


