import React, { useContext } from "react"
import { Link } from "react-router-dom";
import "./soal2.css"

const Nav = () =>{
  
    return(
        
      <nav>
        <img src={require('./img/logo.png')} style={{width:"200px"}}/>
        <ul>
          <li>
            <Link to="/Home">Home</Link>
          </li>
          <li>
            <Link to="/About">About</Link>
          </li>
          <li>
            <Link to="/MovieList">Movie List</Link>
          </li>
          <li>
            <Link to="/login">Login</Link>
          </li>
        </ul>
      </nav>
    )
  }
  
  export default Nav
  